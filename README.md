# GitHub Profile Finder

_It's a simple project to find GitHub users by their username, that displays the user basic information and the public repositories that he have._

## Structure

The application is routed by the **App Component** and have two main pages: **Home** and **Profile** page. Errors will be handled by a **404 Not Found** and a **500 Internal Server Error** pages.

Management of the application state is handled by the **React Context Api** and have a main provider that holds the elements and requests api data.

Most of the logic will happen at the higher components, as in this case, at the page components.

## Design

The design will be very minimalistic due to the fact that the app will have very few elements, so the approach will be using wide and large elements to fill the void of the window.

The theme will inspired by the GitHub website for the elements and colors. The search page and the search input is inspired by the Google search page with a simple large input to fill the space and the profile page will have similarities to the GitHub layout but with more minimalistic traits and large elements.

The **SCSS** naming convention it will follow the **BEM** (block, element and modifier) methodology. Each component will have the respective `.scss` file in the folder. Only the global and the helpers will be on the styles folder.

# Install and initialize

How to setup the project to run on your machine.

## Clone

Open a terminal window, run:

```shell
$ git clone https://gitlab.com/brunocasanova/github-profile-finder.git
$ cd github-profile-finder
```

then install the required packages.

```shell
$ npm install or yarn install
```

## Development mode

Run in watch mode.

```shell
$ npm start or yarn start
```

And visit `http://localhost:3000/` in your browser.

## Testing

Unit, integration and e2e tests.

#### Unit & integration

_Runs the tests in watch mode. Press `a` to re-run all the tests._

```shell
$ npm test or yarn test
```

#### End-to-End

Runs in browser or a cli mode.

1. Start **development mode** in a terminal window:

   ```shell
   $ npm start or yarn start
   ```

2. Then **open a new terminal window** in the project folder and run browser or headless:

   **Browser mode**:

   ```shell
   $ npm run e2e or yarn e2e
   ```

   - Open Cypress client.
   - Choose the browser of your preference (Electron, Chrome or Firefox).
   - _Recommended: Electron_.

   ***

   **Headless mode (cli)**:

   ```shell
   $ npm run e2ecli or yarn e2ecli
   ```

   - Runs the tests headless(cli).
   - Creates a video of the test at `cypress/videos`.

## Build

Builds a folder that is ready to be deployed.

```shell
$ npm run build or yarn build
```

## Versions

This project was made using the versions:

- **Node.js** `v16.13.0`

- **NPM** `8.1.2`

- **Yarn** `1.22.17`

## Note

If you have any problem installing or initializing the project, try **update to the version** refered in the Versions tab.
