import { render } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { Context } from '../context'
import Header from '../components/Header/Header'

const customRender = (ui, value) => {
  return render(
    <MemoryRouter>
      <Context.Provider value={value}>{ui}</Context.Provider>
    </MemoryRouter>
  )
}

describe('<Header /> Component', () => {
  // Selectors helpers
  const imgSelector = { name: /logo/i }
  const headerSelector = { name: /github profile finder/i }

  // Text helpers
  const headerText = 'GitHub Profile Finder'

  it('should render an image and a header with the correct text', () => {
    const { getByRole } = customRender(<Header />)

    const image = getByRole('img', imgSelector)
    const header = getByRole('heading', headerSelector)

    // image should be on the document
    expect(image).toBeInTheDocument()

    // header should be on the document with the correct text
    expect(header).toBeInTheDocument()
    expect(header.textContent).toBe(headerText)
  })
})
