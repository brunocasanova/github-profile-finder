import { render, cleanup, fireEvent } from '@testing-library/react'
import { act } from 'react-dom/test-utils'
import mockAxios from 'axios'
import HomePage from '../pages/HomePage'
import dummies from './dummies/users'
import { BASE_URL, REPOS_LIMIT } from '../config'
import { AppProvider } from '../context'
import { MemoryRouter } from 'react-router-dom'

const { noReposUser, userWithRepos, repositories: repos } = dummies

// Get user without repos API url
const noReposUserApiUrl = `${BASE_URL}/users/${noReposUser.login}`

// Get user with repos API urls
const userWithReposApiUrl = `${BASE_URL}/users/${userWithRepos.login}`
const reposApiUrl = `${BASE_URL}/users/${userWithRepos.login}/repos?per_page=${REPOS_LIMIT}`

// variable to test the requested url
let testRequestedUrl

// intercept axios requests to the API
const mockGet = jest.spyOn(mockAxios, 'get')

describe('<HomePage /> Page Component', () => {
  // Selectors helpers
  const buttonSelector = { name: /Search/i }
  const phSelector = /Type the username here.../i

  // render component with Context and Router
  const customRender = (ui) => {
    return render(
      <AppProvider>
        <MemoryRouter>{ui}</MemoryRouter>
      </AppProvider>
    )
  }

  beforeEach(() => {
    // MOCK API
    mockGet.mockImplementation((url) => {
      testRequestedUrl = url

      // retrieve data by checking the correct url
      switch (url) {
        // if url is from user [without] repos
        case noReposUserApiUrl:
          return Promise.resolve({ data: noReposUser })

        // if url is from user [with] repos
        case userWithReposApiUrl:
          return Promise.resolve({ data: userWithRepos })

        // if url is to get the user repos
        case reposApiUrl:
          return Promise.resolve({ data: repos })

        // throw error if url doesnt match
        default:
          return Promise.resolve(() => {
            throw new Error("Url didn't match")
          })
      }
    })
  })

  // clean up after test is done
  afterEach(() => {
    cleanup()
    jest.clearAllMocks()
  })

  test('should make one request (only) to the API to the correct url after submiting', async () => {
    const { getByPlaceholderText, getByRole } = customRender(<HomePage />)

    const input = getByPlaceholderText(phSelector)
    const button = getByRole('button', buttonSelector)

    await act(async () => {
      // if user type on the input
      await fireEvent.change(input, { target: { value: noReposUser.login } })

      // submit by clicking the button
      fireEvent.click(button)
    })

    // make one request to the api
    expect(mockGet).toBeCalledTimes(1)
    expect(mockGet).toHaveBeenCalledWith(noReposUserApiUrl)
    // confirm if the the url requested should be the right one
    expect(noReposUserApiUrl).toBe(testRequestedUrl)
  })

  test('should make two requests to the API if the user have public repos', async () => {
    const { getByPlaceholderText, getByRole } = customRender(<HomePage />)

    const input = getByPlaceholderText(phSelector)
    const button = getByRole('button', buttonSelector)

    await act(async () => {
      // if user type on the input
      await fireEvent.change(input, { target: { value: userWithRepos.login } })

      // submit by clicking the button
      fireEvent.click(button)
    })

    // user have repos, that means that we should have two requests to the api
    expect(mockGet).toBeCalledTimes(2)
  })
})
