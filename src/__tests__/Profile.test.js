import { render, cleanup } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import Profile from '../components/Profile/Profile'
import dummies from './dummies/users'
import { Context } from '../context'

// mock use navigate method from react router
const mockUseNavigate = jest.fn()

// Mock react router hooks
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // for all non-hook parts
  useNavigate: () => (url) => mockUseNavigate(url),
}))

const customRender = (ui, value) => {
  return render(
    <MemoryRouter>
      <Context.Provider value={value}>{ui}</Context.Provider>
    </MemoryRouter>
  )
}

const { noReposUser, userWithRepos, repositories } = dummies

// structure mock context to have user and repos info
const mockContextWithRepos = {
  userState: [userWithRepos],
  reposState: [repositories.repos],
  isLoadingState: [false],
}

describe('<Profile /> Component', () => {
  afterEach(cleanup)

  test('should render the profile with the correct text', () => {
    const { getByRole } = customRender(<Profile />, mockContextWithRepos)

    // element selectors
    const returnLink = getByRole('link', {
      name: /back to search/i,
    })
    const name = getByRole('heading', {
      name: new RegExp(userWithRepos.name, 'g'),
    })
    const profileImg = getByRole('img', {
      name: /user-logo/i,
    })

    // return to search page link should exist
    expect(returnLink).toBeInTheDocument()

    // heading should exist and the name should match with the props
    expect(name).toBeInTheDocument()
    expect(name.textContent).toBe(userWithRepos.name)

    // profile image should exist
    expect(profileImg).toBeInTheDocument()
  })

  test("should show a message if the user doesn't have any public repos", () => {
    // mock state data with a user that doesnt have any repos for this purpose
    const { getByText } = customRender(<Profile />, {
      userState: [noReposUser],
      reposState: [[]], // means, empty repos
      isLoadingState: [false],
    })

    // message if the user doesnt have any repository to show
    const message = getByText(/user doesn't have any public repository\./i)

    // message should exist and the name should match with the props
    expect(message).toBeInTheDocument()
  })

  test('should show a list of repos if the user have public repos available', () => {
    const { getByRole, container } = customRender(
      <Profile />,
      mockContextWithRepos
    )

    const reposHeader = getByRole('heading', {
      name: /repositories/i,
    })
    const nrOfRepos = container.getElementsByClassName('profile__count')[0]
    const reposItems =
      container.getElementsByClassName('profile__list')[0].children

    // repositories header should exist
    expect(reposHeader).toBeInTheDocument()

    // number of repos should exist
    expect(nrOfRepos).toBeInTheDocument()

    // number of repos should be equal to the user repos
    expect(+nrOfRepos.textContent).toBe(userWithRepos.public_repos)

    // number of repos should be the same number as the public repos
    expect(reposItems).toHaveLength(userWithRepos.public_repos)
  })
})
