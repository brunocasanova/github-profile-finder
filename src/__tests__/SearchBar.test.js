import { render, cleanup, fireEvent } from '@testing-library/react'
import { act } from 'react-dom/test-utils'
import SearchBar from '../components/SearchBar/SearchBar'
import { AppProvider } from '../context'
import { MemoryRouter } from 'react-router-dom'

// mock use navigate method from react router
const mockUseNavigate = jest.fn()

// Mock react router hooks
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // for all non-hook parts
  useNavigate: () => (url) => mockUseNavigate(url),
}))

describe('<SearchBar /> Component', () => {
  // Selectors helpers
  const buttonSelector = { name: /Search/i }
  const phSelector = /Type the username here\.../i
  const msgSelector = /search a github profile by username\./i

  // Text helpers
  const initialMessage = 'Search a GitHub profile by username.'
  const exampleText = 'sometext'
  const exampleUsername = 'username123'

  // render component with Context and Router
  const customRender = (ui) => {
    return render(
      <AppProvider>
        <MemoryRouter>{ui}</MemoryRouter>
      </AppProvider>
    )
  }

  // clean up after test is done
  afterEach(cleanup)

  test('should render an input, button and a message with correct display', () => {
    const { getByPlaceholderText, getByRole, getByText } = customRender(
      <SearchBar />
    )
    const input = getByPlaceholderText(phSelector)
    const button = getByRole('button', buttonSelector)
    const message = getByText(msgSelector)

    // input must be on the document and must be enabled and empty
    expect(input).toBeInTheDocument()
    expect(input).toBeEnabled()
    expect(input.value).toBe('')

    // Focus should be on the input when rendered
    expect(input).toBe(document.activeElement)

    // button must be on the document with correct text
    // should be disabled while the input is empty
    expect(button).toBeInTheDocument()
    expect(button.textContent).toBe('Search')
    expect(button).toBeDisabled()

    // message must be on the document and have the correct text
    expect(message).toBeInTheDocument()
    expect(message.textContent).toBe(initialMessage)
  })

  test('should enable the button after text is inserted in the input', () => {
    const { getByPlaceholderText, getByRole } = customRender(<SearchBar />)
    const input = getByPlaceholderText(phSelector)
    const button = getByRole('button', buttonSelector)

    // input must be empty and button should be disabled
    expect(input.value).toBe('')
    expect(button).toBeDisabled()

    // if user type on the input
    fireEvent.change(input, { target: { value: exampleText } })

    // input should have the value of the text typed on the input
    expect(input.value).toBe(exampleText)

    // button should be enabled after user typed on the input
    expect(button).toBeEnabled()
  })

  test('should set search query after submitting', async () => {
    const { getByPlaceholderText, getByRole } = customRender(<SearchBar />)
    const input = getByPlaceholderText(phSelector)
    const button = getByRole('button', buttonSelector)

    await act(async () => {
      // if user type on the input
      await fireEvent.change(input, { target: { value: exampleUsername } })

      // submit by clicking the button
      fireEvent.click(button)
    })

    // useNavigate should be called 1 time and with the query params
    expect(mockUseNavigate).toBeCalledTimes(1)
    expect(mockUseNavigate).toHaveBeenCalledWith(`?search=${exampleUsername}`)
  })
})
