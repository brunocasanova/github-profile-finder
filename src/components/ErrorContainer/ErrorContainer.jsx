import Button from '../Button/Button'

const ErrorContainer = ({ code, title, message }) => {
  return (
    <div className='error'>
      <div className='error__title'>
        <h1>{code}</h1>
        <h2>{title}</h2>
      </div>

      <div className='error__description'>
        <p>{message}</p>
      </div>
      <Button to='/' clx='error__button' value='Back to Home' />
    </div>
  )
}

export default ErrorContainer
