import { useState, useEffect, useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { AppContext } from '../../context'

const Search = () => {
  const {
    isLoadingState: [, setIsLoading],
    searchMsgState: [searchMsg],
  } = AppContext()
  const [searchText, setSearchText] = useState('')
  const inputRef = useRef(null)
  let navigate = useNavigate()

  useEffect(() => {
    // set the focus on the input on mounting
    inputRef.current?.focus()
  }, [inputRef])

  const getProfileData = ({ code }) => {
    // if the key isn't the "Enter" key or we dont have text, do nothing..
    if ((code && code !== 'Enter') || searchText === '') {
      return
    }

    // Lets show that we are loading the data to the user
    setIsLoading(true)

    // set query to lower case
    const query = searchText.toLowerCase()

    // redirect to profile page with query param
    navigate(`?search=${query}`)
  }

  return (
    <div className='search'>
      <div className='search__bar'>
        <input
          type='text'
          placeholder='Type the username here...'
          ref={inputRef}
          onKeyDown={getProfileData}
          onChange={({ target }) => setSearchText(target.value)}
          value={searchText}
          className={`search__input${
            searchMsg !== '' ? ' search__input--error' : ''
          }`}
        />
        <button
          type='submit'
          className='search__submit'
          disabled={!searchText}
          onClick={getProfileData}
        >
          Search
        </button>
      </div>

      <div className='search__message'>
        <p>
          {searchMsg === ''
            ? `Search a GitHub profile by username.`
            : searchMsg}
        </p>
      </div>
    </div>
  )
}

export default Search
