import Header from '../Header/Header'

const Layout = ({ children }) => {
  return (
    <div className='layout'>
      <Header />
      <div className='layout__container'>{children}</div>
    </div>
  )
}

export default Layout
