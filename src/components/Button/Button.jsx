import { Link } from 'react-router-dom'

const Button = ({ to, clx, value }) => (
  <Link to={to} className={`button ${clx}`}>
    {value}
  </Link>
)

export default Button
