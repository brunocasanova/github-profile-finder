import { appName } from '../../config'
import GitHubLogo from '../../assets/github-logo.svg'

const Header = () => {
  return (
    <div className='header'>
      <img className='header__logo' src={GitHubLogo} alt='logo' />
      <h1 className='header__title'>{appName}</h1>
    </div>
  )
}

export default Header
