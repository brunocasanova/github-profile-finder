import { AppContext } from '../../context'
import RepoItem from '../RepoItem/RepoItem'
import Button from '../Button/Button'

const Profile = () => {
  const {
    userState: [{ displayName, avatar_url, public_repos }],
    reposState: [repos],
  } = AppContext()

  return (
    <div className='profile'>
      <Button to='/' clx='profile__return' value='Back to search' />

      <div className='profile__presentation'>
        <h2>{displayName}</h2>
        <img src={avatar_url} alt='user-logo' />
      </div>

      {public_repos ? (
        <div className='profile__repos'>
          <h3>
            Repositories <span className='profile__count'>{public_repos}</span>
          </h3>
          <div className='profile__list'>
            {repos.map((repo) => (
              <RepoItem {...{ repo }} key={repo.id} />
            ))}
          </div>
        </div>
      ) : (
        <p className='profile__msg'>
          User <b>{displayName}</b> doesn't have any public repository.
        </p>
      )}
    </div>
  )
}

export default Profile
