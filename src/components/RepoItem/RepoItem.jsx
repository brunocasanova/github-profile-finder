const RepoItem = ({ repo: { html_url, name, description } }) => (
  <div className='item'>
    <a className='item__title' href={html_url} target='_blank' rel='noreferrer'>
      {name}
    </a>
    <p className='item__description'>
      <i>{description || `No description`}</i>
    </p>
  </div>
)

export default RepoItem
