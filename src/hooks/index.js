import { useMemo } from 'react'
import { useLocation } from 'react-router-dom'

// custom hook to parse the query string
export const useQuery = () => {
  const { search } = useLocation()
  return useMemo(() => new URLSearchParams(search), [search])
}
