import { useState, createContext, useContext } from 'react'

export const Context = createContext()
export const AppContext = () => useContext(Context)

export const initialUser = {
  displayName: '',
  name: '',
  login: '',
  avatar_url: '',
  public_repos: 0,
}

export const AppProvider = ({ children }) => {
  const userState = useState(initialUser)
  const reposState = useState([])
  const isLoadingState = useState(false)
  const searchMsgState = useState('')

  const value = {
    userState,
    reposState,
    isLoadingState,
    searchMsgState,
  }

  return <Context.Provider {...{ value }}>{children}</Context.Provider>
}
