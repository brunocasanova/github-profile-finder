import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { AppProvider } from './context'
import Layout from './components/Layout/Layout'
import HomePage from './pages/HomePage'
import ProfilePage from './pages/ProfilePage'
import NotFoundPage from './pages/NotFoundPage'
import ServerErrorPage from './pages/ServerErrorPage'

const history = createMemoryHistory()

const App = () => (
  <AppProvider>
    <Layout>
      <Router history={history}>
        <Routes>
          <Route path='/' exact element={<HomePage />} />
          <Route path='/home' exact element={<Navigate to='/' />} />
          <Route path='profile/:username' element={<ProfilePage />} />
          <Route path='server-error' element={<ServerErrorPage />} />
          <Route path='*' element={<NotFoundPage />} />
        </Routes>
      </Router>
    </Layout>
  </AppProvider>
)

export default App
