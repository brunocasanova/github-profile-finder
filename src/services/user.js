import { REPOS_LIMIT } from '../config'
import axios from 'axios'
import { BASE_URL } from '../config'

export const getUser = async (query) => {
  const {
    data: { name, login, avatar_url, public_repos },
  } = await axios.get(`${BASE_URL}/users/${query}`)

  // if the user doesnt have a name set, display the username
  const displayName = name || login

  return { displayName, name, login, avatar_url, public_repos }
}

export const getRepos = async (username) => {
  const { data } = await axios.get(
    `${BASE_URL}/users/${username}/repos?per_page=${REPOS_LIMIT}`
  )

  return data
}
