// Basic configs
export const appName = 'GitHub Profile Finder'

// Some api default configs and options
export const BASE_URL = `https://api.github.com`
export const REPOS_LIMIT = 100
