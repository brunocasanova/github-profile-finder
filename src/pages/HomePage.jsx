import { useEffect, useCallback } from 'react'
import { useNavigate } from 'react-router-dom'
import { AppContext } from '../context'
import { getUser, getRepos } from '../services/user'
import { useQuery } from '../hooks'
import SearchBar from '../components/SearchBar/SearchBar'
import Spinner from '../components/Spinner/Spinner'

const HomePage = () => {
  const {
    userState: [, setUser],
    reposState: [, setRepos],
    searchMsgState: [searchMsg, setSearchMsg],
    isLoadingState: [isLoading, setIsLoading],
  } = AppContext()
  let navigate = useNavigate()

  // get the query string form the hook
  let query = useQuery()
  const username = query.get('search')

  const getParamsData = useCallback(
    async (query) => {
      try {
        // lets make an api call to get the user data
        const userData = await getUser(query)
        await setUser(userData)

        // if the user have any public repo, lets get the repos
        if (userData?.public_repos > 0) {
          const repos = await getRepos(userData.login)
          await setRepos(repos)
        }

        // we have the data ready, stop the loading screen
        setIsLoading(false)

        // redirect to profile page
        navigate(`/profile/${username}`)
      } catch (error) {
        // We doesn't found a result here, stop the loading screen
        setIsLoading(false)

        // if the response is 404, that user doesn't exist
        if (error?.response?.status === 404) {
          setSearchMsg(`GitHub user doesn't exist, try again please.`)

          // redirect to home page
          return navigate('/')
        } else {
          // Server down or we reached the api requests limit
          // redirect to server error page
          return navigate('/server-error')
        }
      }
    },
    [setIsLoading, setSearchMsg, setUser, setRepos, navigate, username]
  )

  useEffect(() => {
    if (username) {
      // Lets get the data from the params
      getParamsData(username)
    }

    // after the component is unmounted, let's clear
    return () => {
      // if our last result was not found, lets clear the error message
      if (searchMsg !== '') {
        setSearchMsg('')
      }
    }
  }, [searchMsg, setSearchMsg, getParamsData, username])

  if (isLoading) {
    return <Spinner />
  }

  return <SearchBar />
}

export default HomePage
