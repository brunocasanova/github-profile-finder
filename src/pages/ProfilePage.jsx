import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import Profile from '../components/Profile/Profile'
import { AppContext, initialUser } from '../context'

const ProfilePage = () => {
  const {
    userState: [{ displayName }, setUser],
    reposState: [, setRepos],
  } = AppContext()
  let navigate = useNavigate()

  useEffect(() => {
    // if we don't have data redirect to home page
    if (!displayName) {
      return navigate('/')
    }

    // after the component is unmounted, reset the state to the initial
    return () => {
      setUser(initialUser)
      setRepos([])
    }
  }, [navigate, setUser, setRepos, displayName])

  return <Profile />
}

export default ProfilePage
