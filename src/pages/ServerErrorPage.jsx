import ErrorContainer from '../components/ErrorContainer/ErrorContainer'

const ServerErrorPage = () => (
  <ErrorContainer
    code='500'
    title='Internal Server error'
    message='Server error. We are sorry, try again later.'
  />
)

export default ServerErrorPage
