import ErrorContainer from '../components/ErrorContainer/ErrorContainer'

const NotFoundPage = () => (
  <ErrorContainer
    code='404'
    title='Page Not Found'
    message="The page you were looking for doesn't exist."
  />
)

export default NotFoundPage
