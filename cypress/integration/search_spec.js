/**
 *  E2E test to search for a github profile
 *
 *  - Let's confirm:
 *    - that the error is displaying if the user isn't found
 *    - that the loading spinner appear to the user
 *    - if the user go to the profile page that he is searching
 *    - that the user can go back from the profile page, to the search page
 * */

describe('Search for a GitHub profile', () => {
  const url = 'http://localhost:3000/'
  const testUser = 'torvalds'
  const searchButton = { name: /search/i }
  const waitTime = { timeout: 60000 }

  it('User get an error if the result is not found', () => {
    // visit page
    cy.visit(url)

    // type a username on the search input
    cy.findByRole('textbox').type('thisperson435should324not98exist')

    // submit the search by clicking the submit button
    cy.findByRole('button', searchButton).click()

    // user check that the user doesn't exist in the message
    cy.findByText(/github user doesn't exist, try again please\./i, waitTime)
  })

  it('User can see that the data is being loaded', () => {
    // visit page
    cy.visit(url)

    // type a username on the search input
    cy.findByRole('textbox').type(testUser)

    // submit the search by clicking the submit button
    cy.findByRole('button', searchButton).click()

    // user check in the message field the loading spinner
    cy.findByText(/loading\.\.\./i)
  })

  it('User can visit the profile page', () => {
    // visit page
    cy.visit(url)

    // type a username on the search input
    cy.findByRole('textbox').type(testUser)

    // submit the search by clicking the submit button
    cy.findByRole('button', searchButton).click()

    // user check the profile page name
    cy.findByRole('heading', {
      ...waitTime,
      name: /linus torvalds/i,
    })
  })

  it('User can go back from the profile page to home page', () => {
    const returnButton = { name: /back to search/i }

    // visit page
    cy.visit(url)

    // type a username on the search input
    cy.findByRole('textbox').type(testUser)

    // submit the search by clicking the submit button
    cy.findByRole('button', searchButton).click()

    // user check the "return to search" link
    cy.findByRole('link', returnButton)

    // user return back to the search page
    cy.findByRole('link', returnButton).click()

    // user is at the search page and check the initial message
    cy.findByText(/search a github profile by username\./i)

    // user is at the search page and checks the search input
    cy.findByRole('textbox')
  })
})
